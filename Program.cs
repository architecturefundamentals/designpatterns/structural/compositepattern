﻿using CompositePattern;

IHuman mathys = new Child("Mathys");
IHuman erwan = new Parent("Erwan");
IHuman amelie = new Parent("Amelie");


mathys.addParent(erwan);
mathys.addParent(amelie);
erwan.addChild(mathys);
amelie.addChild(mathys);

GrandParent philippe = new GrandParent("Philippe");

philippe.addChild(erwan);
erwan.addParent(philippe);

philippe.show();
erwan.show();
mathys.show();