namespace CompositePattern
{
    public class Child: IHuman
    {
        public String Name{get; set;}
        private List<IHuman> parents = new List<IHuman>();
        public Child(String name){
            this.Name = name;
        }

        public void addChild(IHuman child)
        { 
        }

        public void addParent(IHuman parent)
        {
            parents.Add(parent);
        }

        public string getName()
        {
            return this.Name;
        }

        public void show()
        {
            Console.WriteLine("===========================");
            Console.WriteLine("Name: " + Name);
            Console.WriteLine("Parents: ");
            foreach(IHuman parent in parents){
                Console.WriteLine("     Name: "+ parent.getName());
            }
            Console.WriteLine("===========================");
        }
    }
}