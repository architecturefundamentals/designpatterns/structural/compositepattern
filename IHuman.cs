namespace CompositePattern
{
    public interface IHuman
    {
        public String getName();
        public void show();
        public void addChild(IHuman child);
        public void addParent(IHuman parent);
    }
}