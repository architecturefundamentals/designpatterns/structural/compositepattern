# COMPOSITE PATTERN

---
## A bit of context ...
>### _What is this pattern about?_
>This pattern it's a creational pattern, it  lets you compose objects into tree structures and then work with these structures as if they were individual objects.
>
> It's usually used in the next cases:
>  * When you have to implement a tree-like object structure.
>  * When you want the client code to treat both simple and complex elements uniformly.
>
---
## But this simple example is about...
This example was taken from [Medium](https://medium.com/javarevisited/composite-design-pattern-java-9cd0964d3b12)

![Demo](img/composite.png)

and Done!
:smile:
---
_Tech: C#_ 
