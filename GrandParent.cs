namespace CompositePattern
{
    public class GrandParent : IHuman
    {
        public String Name{get; set;}
        private List<IHuman> childs = new List<IHuman>();
         private List<IHuman> grandParents = new List<IHuman>();
        public GrandParent(String name){
            this.Name = name;
        }

        public void addChild(IHuman child)
        {
            childs.Add(child);
        }

        public void addParent(IHuman parent)
        {
            
        }

        public string getName()
        {
            return this.Name;
        }

        public void show()
        {
            Console.WriteLine("===========================");
            Console.WriteLine("Name: " + Name);
            Console.WriteLine("Parents: ");
            foreach(IHuman parent in grandParents){
                Console.WriteLine("     Name: "+ parent.getName());
            }
            Console.WriteLine("Childs: ");
            foreach(IHuman child in childs){
                Console.WriteLine("     Name: "+ child.getName());
            }
            Console.WriteLine("===========================");
        }
    }
}